# c3resPatcher v0.5.4

*Play Caesar 3 with any resolution monitor*

To download the latest build, check out [tags](https://gitlab.com/Afdch/c3respatcher/tags)

## Features

* choose any resolution you want (this program will try to detect your (main) monitor resolution)
* world map display is fixed on higher resolutions (all borders and cities now should be in place)
* not resolution-related, but this now also fixes the missing audio for losing a mission and a prefect fighting enemies voice line

## How to use:

1. Place c3resolutionChanger.exe and c3.data in any location on your PC.
1. Launch c3resolutionChanger.exe
1. Specify required resolution if needed and press Patch! button. Make sure you don't have the exe with required resolution already. If the patcher says you do, rename or delete the executable with that name.
1. Patcher would generate a c3 executable with specified resolution. Place generated executable into your Caesar 3 folder.
1. After loading the game, select "1024x768" resolution in the game settings.

Make sure you don't have any upscaling factors enabled in your system

## Adjusting for use in a windowed mode

It is possible to run Caesar 3 in a windowed mode. However if you will decide to switch to a windowed screen from the game display settings, you will get an error message telling you that it is not possible to switch.
It is quite possible though, but to do that, you will need to perform several additional steps.
1. Using this program, make a c3 exe with a slightly less resolution than your monitor's. Checking "Adjust for running in a windowed mode" will automatically subtract 30 pixels from each of the horisontal and vertical resolutions specified earlier. Checking "Maintain aspect ratio" will further reduce the resolution to keep you monitor's aspect ratio detected earlier. If you're not using automatically guessed values on a previous step for whatever reason, it is not necessary to check these boxes.
1. Right click on the executable, go to Properties -> Compatibility -> Settings, check Reduced colour mode and select 16-bit (65536) colour. Press OK.
1. Start the Caesar 3 (restart, if you have a black screen). Caesar should run in a 640x480 mode. Go to options-> display settings and select windowed screen. Go to display settings again and select 1024 by 768 resolution. Restart Caesar 3.

Now the game should be running in a windowed mode. Note that the width should be divisible by 4 for colour adjustment to work.
To run it in a borderless windowed mode, try https://github.com/Raymai97/noborder/releases or any other no-border program of your choice.

## Acknowledgment

Many thanks for those who were there before me - jackfruste, Space_man and Crudelios.

## Known bugs

* The sprite of a selected unit may be not displayed properly.
* World map might not scroll properly with some weird resolutions ((x > 2030 and y < 1080 ) or (x < 2030 and y > 1080)). If you *really* need that resolution, write me and I'll try to fix it as well.