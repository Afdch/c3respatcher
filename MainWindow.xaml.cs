﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;

namespace c3resolutionChanger
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static int xRes;                    //horizontal resolution -   in px
        static int yRes;                    //vertical resolution   -   in px
        //static (int, int) resolution; no tuples yet
        static int colCount;                //number of columns     -   in #
        static int rowCount;                //number of rows        -   in #
        static int sidePanelsNumber;        //required number of additional side panels
        static int topPanelsNumber;         //required number of additional (?) top panel segments
         
        static int sidePanelShift;          //shift for a side panel -  in px
        static int sidePanelShiftCollapsed; //shift for a collapsed side panel controls -  in px
        static int advisorScreenShiftX;     //shift for a image behind advisor screen and other UI windows to center the element
        static int advisorScreenShiftY;     //shift for a image behind advisor screen and other UI windows to center the element

        static BinaryReader BinaryReader;
        static BinaryWriter BinaryWriter;
        static string fileName = $""; 

        static string newFilePath = "";
        static string oldFilePath =    System.IO.Path.Combine(Directory.GetCurrentDirectory(), "c3.data");

        Regex regex = new Regex($"[A-Za-z0-9]");

        string c3VanillaChecksumSHA256Managed = "04C4177F189A2257B5E11D8095EC549F4DDB1D89290C8080617B61175E47D74E";

        bool buttonWasClicked = false;
        /// <summary>
        /// Describes what will patch! button do
        /// </summary>
        string patchDescription;
        static bool isLoaded = false;

        byte[] ReverseInt(int value)
        {
            byte[] newByte = BitConverter.GetBytes(value);
            Array.Reverse(newByte);
            return newByte;
        }


        public MainWindow()
        {
            InitializeComponent();

            this.xResValue.Text = (System.Windows.SystemParameters.PrimaryScreenWidth).ToString();
            this.yResValue.Text = (System.Windows.SystemParameters.PrimaryScreenHeight).ToString();
            DetermineResolution();
            isLoaded = true;

        }

        private void UpdateDescription()
        {
            this.DescriptionTextBlock.Text = $"Will generate {xRes} by {yRes} Caesar 3 executable";
        }

        private static byte[] GetBytes(string byteString)
        {
            string regString = Regex.Replace(byteString, @"[^A-Fa-f0-9]", "");
            int stringLen = regString.Length;
            if (stringLen % 2 == 1) throw new Exception("Wrong string to get bytes from");
            byte[] newbyte = new byte[stringLen / 2];
            for (int i = 0; i < stringLen / 2; i ++)
            {
                newbyte[i] = (byte)Convert.ToInt32($"0x{regString[i*2]}{regString[i*2+1]}", 16);
            }

            return newbyte;
        }
        
        /// <summary>
        /// Determines the difference between the caller address and the called adress
        /// </summary>
        /// <param name="callFrom">Address of the caller</param>
        /// <param name="callTo">Address of the called function</param>
        /// <returns>A byte array with a correct OP codes for the call</returns>
        static byte[] GetFunctionCallOffset(int callFrom, int callTo)
        {
            int difference = callTo - callFrom - 5;
            string differenceString = difference.ToString("x8");
            byte[] newByte = GetBytes(differenceString);
            Array.Resize(ref newByte, 5);
            newByte[4] = callTo - callFrom < 0 ? (byte)0xe8 : (byte)0xe9; //if calling back
            Array.Reverse(newByte);
            return newByte;
        }

        /// <summary>
        /// Revverses the order of 4 byte word represented as a 8 character string
        /// each byte is represented by 2 chars in HEX format
        /// </summary>
        /// <param name="original">Original string</param>
        /// <returns>A string in reversed byte order</returns>
        static string GetReversedAddressString(string original)
        {
            char[] newString = new char[8];
            newString[0] = original[6];
            newString[1] = original[7];
            newString[2] = original[4];
            newString[3] = original[5];
            newString[4] = original[2];
            newString[5] = original[3];
            newString[6] = original[0];
            newString[7] = original[1];

            return new string(newString);
        }

        /// <summary>
        /// Revverses the order of 4 byte word represented as a 8 character string
        /// the value is expressed in the integer format
        /// </summary>
        /// <param name="original">the integer value to be represented in reversed hex format string</param>
        /// <returns>A string in reversed byte order</returns>
        static string GetReversedAddressString(int original) => GetReversedAddressString(original.ToString("x8"));

        static string GetFunctionOffsetString(int callFrom, int callTo)
        {
            int difference = callTo - callFrom - 5;
            string differenceString = difference.ToString("x8");
            return GetReversedAddressString(differenceString);
        }


        /// <summary>
        /// Determines the difference between the caller address and the called adress
        /// and writes the result at the caller address
        /// </summary>
        /// <param name="callFrom">Address of the caller</param>
        /// <param name="callTo">Address of the called function</param>
        static void WriteFunctionOffset(int callFrom, int callTo) => Write(callFrom, GetFunctionCallOffset(callFrom, callTo));


        private void DoTheMagic()
        {


            //resolutions - in pixels
            DetermineResolution();
            //calculate required values for tile offset and image draws
            GenerateFile();

            //check for file and create if necessary
            if (GetChecksum(oldFilePath) != c3VanillaChecksumSHA256Managed)
            {
                Console.WriteLine("Wrong checksum! Check your c3.exe");
                this.PatchButton.Content = "Wrong checksum! Check your c3.exe";
            } else 
            if (!CreateNewC3exe())
            {
                Console.WriteLine("File already exists");
                this.PatchButton.Content = "Error! File already exists. Exit";
            }
            else
            {
                //HEXEDIT MAGIC
                ModifyCreatedC3exe();
                this.PatchButton.Content = "Success! Exit";
            }

        }


        private void ModifyCreatedC3exe()
        {
            //Write(0x28f9, GetBytes("e9adb01100 e9e5b01100"));
            {
                WriteFunctionOffset(0x28f9, 0x11d9ab);
                WriteFunctionOffset(0x28fe, 0x11d9e8);
                Write(0xce780, GetReversedAddressString(xRes));
                Write(0xCE78a, GetReversedAddressString(yRes));
                Write(0xCEa0d, GetReversedAddressString(xRes));
                Write(0xCEa17, GetReversedAddressString(yRes));

                // number of columns and rows
                // see below for additional conditional changes 
                Write(0xd5403, GetReversedAddressString(xRes));
                Write(0xd540e, GetBytes(rowCount.ToString("x2")));
                Write(0xd5410, GetBytes((colCount + 2).ToString("x2")));
                Write(0xd54aa, GetReversedAddressString(xRes));
                Write(0xd54b5, GetBytes(rowCount.ToString("x2")));
                Write(0xd54b7, GetBytes(colCount.ToString("x2")));

                //resolution, collapsed and opened side panel
                Write(0xfe7ed, GetReversedAddressString(xRes));
                Write(0xfe7f9, GetReversedAddressString(sidePanelShift));
                Write(0xfe803, GetReversedAddressString(sidePanelShiftCollapsed));

                //some weird resolution check, maybe we don't need it?
                Write(0xfe812, GetReversedAddressString(xRes));
                Write(0xfe836, GetReversedAddressString(xRes));

                // we probably do need this though 
                Write(0xfe909, GetReversedAddressString(xRes));

                // additional side panels draw call
                // See below for the inserted function
                WriteFunctionOffset(0xfe92f, 0x28fe);
                WriteFunctionOffset(0xfe958, 0x28fe);

                // some unknown resolution uses
                Write(0xff5ec, GetReversedAddressString(xRes));
                Write(0xffca1, GetReversedAddressString(xRes));
                Write(0xffcad, GetReversedAddressString(xRes - 24)); //24? Why? maybe 20? 40? 0?


                // something about drawing additional sidepanels 
                // during the collapsing and restoring side control
                Write(0xffdf8, GetReversedAddressString(xRes));
                WriteFunctionOffset(0xffe15, 0x28fe);
                WriteFunctionOffset(0xffe36, 0x28fe);
                //WriteSidePanelOffset(0xffe15);
                //WriteSidePanelOffset(0xffe36);


                //advisor background image 
                //(and the same model for other screens)
                WriteFunctionOffset(0x1013b6, 0x28f9);
                WriteFunctionOffset(0x1013d6, 0x28f9);
                WriteFunctionOffset(0x101419, 0x28f9);
                WriteFunctionOffset(0x101458, 0x28f9);
                WriteFunctionOffset(0x101478, 0x28f9);

                WriteFunctionOffset(0x1020c4, 0x28f9);
                WriteFunctionOffset(0x102252, 0x28f9);
                WriteFunctionOffset(0x102e9f, 0x28f9);
                WriteFunctionOffset(0x103068, 0x28f9);



                //top panel function
                //Write(0x11b34b, GetBytes()); do not change this!!

                // The idea for part was completely borrowed from Crudelios' changes to 2560x1440 resolution patch
                // had to change the jump condition due to limitations (JGE is for signed int and works under 127 tiles,
                // which we need to exceed on higher resolutions

                Write(0x11c3f0, GetReversedAddressString(xRes));
                Write(0x11c400, GetBytes("00"));                // I don't know why -> Crudelios
                Write(0x11c415, GetBytes("80"));                // Change CMP 32 to CMP 8
                Write(0x11c419, GetBytes("73"));                // Change JGE to JAE(JNB)
                Write(0x11c418, GetBytes(topPanelsNumber.ToString("x2"))); //this might exceed 127, with JAE it can go up to 0xff, i. e. 6144 pixels wide 
                                                                           //this can probably be made even bigger but would require more insertions in the code

                Write(0x11c465, GetBytes("9090"));              //disable jump, how -> Crudelios

                //narrow side panel draw
                Write(0x11d699, GetBytes(GetReversedAddressString(sidePanelShift + 162)));
                Write(0x11d6bc, GetBytes(GetReversedAddressString(yRes)));
            }

            /* ============= UGLY AREA ============= *\
            |      the text of inserted functions     |
            |          on higher resolutions          |
            \* ===================================== */
            {
                // inserted function 1. shifts bg images for advisor screen etc
                // bg images are 1024x768, 

                string insertion11 = $"8b3c2483c404813df4c95b00";
                string insertion12 = GetReversedAddressString(xRes);       // needed X resolution in this format

                string insertion13 = $"7510";
                string insertion14 = $"81442404{GetReversedAddressString(advisorScreenShiftX)}";       // advisorScreenShiftX
                string insertion15 = $"81442408{GetReversedAddressString(advisorScreenShiftY)}";       // advisorScreenShiftY

                string insertion16 = $"e81147eeff57c3";             //j_image_draw

                string insertion1 = insertion11 + insertion12 + insertion13 + insertion14 + insertion15 + insertion16;
                Write(0x11d9ab, GetBytes(insertion1));

                //inserted function 2, draws additional side panels on on the right
                //until addtional images exceed the vertical resolution
                string insertion21 = $"558becff7510ff750cff7508";
                string insertion22 = $"e8ea46eeff";                 //j_image_draw call
                string insertion23 = $"8145fc1d010000817dfc";
                string insertion24 = GetReversedAddressString(yRes);       // resolution Y
                string insertion25 = $"7eeb8be55dc3";

                string insertion2 = insertion21 + insertion22 + insertion23 + insertion24 + insertion25;
                Write(0x11d9e8, GetBytes(insertion2));
            }


            /* ============= TEST AREA ============= *\
            |        fixes the world map render        |
            |          on higher resolutions          |
            \* ===================================== */

            //note 1920 and 1080 in this case are really roughly estimated values
            // but I would need a bigger than 1080p monitor to identify the exact values 
            // at which we need to go away from these estimated numbers
            // it is probably something like 2000 + borders of the map screen
            //                           and 1000 + the hight of borders city selection UI

            //if the values are exceeded, use constant values instead of variables
            //int the original function

            if (xRes > 2030 || yRes > 1080)
            {
                //replace xRight
                Write(0x11321d, "b9f007000090"); // 0x7f0 = 2032    | MOV exc, xRight -> MOV eax, 2032
                //Write(0x3fc54, "b9f007000090"); // 0x7f0 = 2032     | MOV exc, xRight -> MOV eax, 2032
                Write(0xd2e21, "9090909090"); //Block scrolling on the map area
                Write(0xd2c05, "9090909090");
                //Write(0x11321e, GetReversedAddressString(1000));
                /*Write(0x14fb60, GetReversedAddressString(xRes+1895));
                Write(0x113244, GetReversedAddressString(xRes+1895));
                Write(0x113257, GetReversedAddressString(xRes+1895));*/
  
                Write(0x11322b, "ba7404000090"); // 0x440 = 1088    | MOV edx, yBottom -> MOV edx, 1088
                //Write(0x3fc9f, "b874040000"); // 0x440 = 1088      | MOV eax, yBottom -> MOV eax, 1088

                /*Write(0x14fb60, GetReversedAddressString(yRes+900));
                Write(0x11326b, GetReversedAddressString(yRes+900));
                Write(0x11327e, GetReversedAddressString(yRes+900));*/
            }


            /* ============ TEST AREA 2 ============ *\
            |        fixes the number of tiles         |
            |           displayed on creen            |
            \* ===================================== */

            // this erases the "default" case as it is unreachable anyway
            // had to do this because in the code the 60 OP code was used for PUSH an imm8 value
            // substituted with 68 command for PUSH imm32 value

            if (rowCount >= 127 || colCount >= 127)
            {

                // PUSH 0x1e PUSH 0x3a PUSH rowCount PUSH (colCount + 2) PUSH 0x18 PUSH 00 PUSH 0xa2 PUSH 0xa2
                string push = $"6a1e 6a3a 68{GetReversedAddressString(rowCount)} 68{GetReversedAddressString(colCount + 2)} 6a18 6a00 68a2000000 68a2000000";

                Write(0xd5407, "90 9090 9090 9090 9090 " +
                    " 9090 9090 9090 9090 9090 9090 9090 9090" +
                    " 9090 9090 9090 9090 9090 9090 9090 9090" +
                    " 9090 9090 9090 9090 9090 9090 9090 90");
                Write(0xd5407, push);

                push = $"6a1e 6a3a 68{GetReversedAddressString(rowCount)} 68{GetReversedAddressString(colCount)} 6a18 6a00 68a2000000 68a2000000";

                Write(0xd54ae, "9090 " +
                    " 9090 9090 9090 9090 9090 9090 9090 9090" +
                    " 9090 9090 9090 9090 9090 9090 9090 9090" +
                    " 9090 9090 9090 9090 9090 9090 9090 9090" +
                    " 9090 9090 9090");
                Write(0xd54ae, push);
               
            }

            /* ============ LOST MISSION =========== *\
            |         plays the audio when the        |
            |             mission is lost             |
            \* ===================================== */
            {
                /* Starting offset: 0x10140d
                 * original code:
                 * //
                 * push 0                   |
                 * push 0                   |
                 * movsx ecx, word_6430e8   |
                 * push ecx                 |
                 * call j_image_draw        |   call inserted_function_3 instead?
                 * add esp, 0ch             |
                 * //
                 * jmp short_loc            <- preserve
                 *
                 * // 6a00 6a00 0fbf15e8306400 83c202 52 e8860cf0ff 83c40c // e838
                 */

                /* need to insert play_sound function and save the code above
                 * j_play_sound is at 0x1a99
                 * takes 3 arguments:
                 * push 0 ; int             : 6a00
                 * push 1 ; dwUser          : 6a01
                 * add eax offset missionFailwWav ;todo: insert that sting somewhere too
                 *                          : 05{offset}
                 * push eax                 : 50
                 * call j_play_sound        : e8{call 0x1a99}
                 * add esp 0Ch              : 83c40c
                 */

                WriteFunctionOffset(0x101419, 0x2903);        //insert j_insert_3 instead j_image_draw

                int loseMissionWavOffset = 0x158700;

                WriteFunctionOffset(0x2903, 0x11da18);  //insert_3

                //================Redo?==================
                WriteFunctionOffset(0x101419, 0x11da18);
                //=======================================

                WriteFunctionOffset(0x11da18, 0x28f9);  //missing j_image_draw : +5 -> missing insertion 1 (for higher resolutions)
                Write(0x11da18 + 5, "83c40c6a006a01b8");
                Write(0x11da18 + 13, "00a15500");               //todo: define offset for this -> GetReversedAddressString(loseMissionWavOffset)
                                                                //loseMissionWavOffset.ToString("x8")
                Write(0x11da18 + 17, "50");
                WriteFunctionOffset(0x11da18 + 18, 0x1a99); //j_play_sound
                Write(0x11da18 + 23, "c3");

                //================Redo?==================
                Write(0x11da18 + 23, "e9ea39feff");
                //=======================================



                /* ============ FIXING AUDIO =========== *\
                |            replace some missing         |
                |             audio to the game           |
                \* ===================================== */

                Write(loseMissionWavOffset, Encoding.ASCII.GetBytes("wavs\\lose_game.wav"));
                Write(0x158ec0, Encoding.ASCII.GetBytes("Vigils_exact10.wav"));

                //testing emigrants
                Write(0x15b908, Encoding.ASCII.GetBytes("Ëmigrate_exact1.wav"));
                Write(0x15b920, Encoding.ASCII.GetBytes("Ëmigrate_exact2.wav"));
                Write(0x15b938, Encoding.ASCII.GetBytes("Ëmigrate_exact3.wav"));
                Write(0x15b950, Encoding.ASCII.GetBytes("Ëmigrate_exact4.wav"));

            }
            
            
        }

        private static string GetChecksum(string file)
        {
            using (FileStream stream = File.OpenRead(file))
            {
                SHA256Managed sha = new SHA256Managed();
                byte[] checksum = sha.ComputeHash(stream);
                return BitConverter.ToString(checksum).Replace("-", String.Empty);
            }
        }

        /// <summary>
        /// Writes byte array byte by byte at the specified position
        /// </summary>
        /// <param name="positionOffset">starting position of data to write</param>
        /// <param name="bytes">byte array containing data</param>
        static void Write(int positionOffset, byte[] bytes)
        {
            using (BinaryWriter = new BinaryWriter(File.OpenWrite(newFilePath)))
            {
                BinaryWriter.BaseStream.Position = positionOffset;
                foreach (byte myByte in bytes)
                {
                    BinaryWriter.Write(myByte);
                }
            }

        }

        static void Wipe(int startingPosition, int endingPosition)
        {
            string wipeString = "";
            for (int postion = 0; postion <= endingPosition - startingPosition; postion++)
            {
                wipeString += "90";
            }
            Write(startingPosition, wipeString);
        }

        /// <summary>
        /// Writes byte array byte by byte at the specified position
        /// </summary>
        /// <param name="positionOffset">starting position of data to write</param>
        /// <param name="strBytes">String representing the byte array containing data</param>
        static void Write(int positionOffset, string strBytes) => Write(positionOffset, GetBytes(strBytes));

        //not used for now
        private byte[] ReadC3exe()
        {
            byte[] myBytes = new byte[100];
            using (BinaryReader = new BinaryReader(File.OpenRead(oldFilePath)))
            {
                int myByte;
                int i = 0;
                    while ( i++ < 50)
                {
                    myBytes[i] = BinaryReader.ReadByte();
                }
            }
            return myBytes;
        }

        /// <summary>
        /// Checks if the file with such a specified resolution exists already
        /// If not, creates a copy of original file
        /// </summary>
        /// <returns>True if the file was created successfully</returns>
        private bool CreateNewC3exe()
        {
            //??
            bool fileCreateSuccess;
            if (!File.Exists(newFilePath))
            {
                File.Copy(oldFilePath, newFilePath);
                fileCreateSuccess = true;
            }
            else
            {
                fileCreateSuccess = false;
            }
            return fileCreateSuccess;
        }

        private void PatchButton_Click(object sender, RoutedEventArgs e)
        {
            if (buttonWasClicked) System.Windows.Application.Current.Shutdown();
            else
            {
                buttonWasClicked = true;
                DoTheMagic();
            }
        }

        private void DetermineResolution()
        {
            xRes = int.Parse(this.xResValue.Text);
            yRes = int.Parse(this.yResValue.Text);


            // check if we create a file for the windowed mode
            if ((bool)this.TryWindowedCheckBox.IsChecked)

            {
                //to run the game in the windowed mode, you have to 
                float screenRatio = (float)xRes / yRes;

                xRes -= 30;
                yRes -= 30;
                if ((bool)this.MaintainRatioCheckBox.IsChecked && this.MaintainRatioCheckBox.IsEnabled )
                {
                    
                    if (xRes > (int)(yRes * screenRatio))
                    {
                        xRes = (int)(yRes * screenRatio);
                        xRes = xRes % 2 == 1 ? xRes - 1 : xRes; 
                    }
                    else
                    {
                        yRes = (int)(xRes / screenRatio);
                    }

                }

            }

            //xRes should be a numerator of 4
            //to work in 16-colour compatibility mode
            xRes = xRes % 4 == 0 ? xRes : (xRes / 4) * 4;

            UpdateDescription();
        }

        private void GenerateFile()
        {


            //pretending c3.data is not a renamed original c3.exe
            //and creating a new c3 "out of at thin air"
            string isWindowedString = (bool)this.TryWindowedCheckBox.IsChecked ? " windowed" : "";
            string fileName = $"c3 ({xRes}x{yRes}){isWindowedString}.exe";
            newFilePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), fileName);

            //TODO: find what determines the with of that narrow panel
            bool narrowSidePanel = true;
            int sidePanelWidth = narrowSidePanel ? 180 : 160;

            //tiles - in tile/block numbers
            colCount = ((xRes - 160) / 60); 
            rowCount = (yRes / 15)  - 1;

            sidePanelShift = colCount * 60 - 2;
            sidePanelShiftCollapsed = (colCount + 2) * 60 - 2;


            sidePanelsNumber = (int)Math.Ceiling((float)(yRes - 475) / 285);
            topPanelsNumber = (int)Math.Floor((float)xRes / 24);
            topPanelsNumber = topPanelsNumber > 225 ? 255 : topPanelsNumber;

            //advisor screen values
            advisorScreenShiftX = xRes > 1024 ? (xRes - 1024) / 2 : 0;
            advisorScreenShiftY = yRes > 768 ? (yRes - 768) / 2 : 0;


        }

        private void TryWindowedCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            this.MaintainRatioCheckBox.IsEnabled = !this.MaintainRatioCheckBox.IsEnabled;
            DetermineResolution();

        }

        private void MaintainRatioCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            DetermineResolution();
        }

        private void TextValue_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            if (isLoaded) DetermineResolution();
        }
    }
}
